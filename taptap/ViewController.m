//
//  ViewController.m
//  taptap
//
//  Created by Alain Pitre on 2016-01-17.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import "ViewController.h"
#import "Game.h"
#import "TapButton.h"
#import "Menu.h"
#import "StateButton.h"

@implementation ViewController {
    Game* game;
    Menu* menu;
    StateButton* state;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepMenu];
    [self prepState];
    
    game = [[Game alloc] init];

    [game addBtn:_btn1];
    [game addBtn:_btn2];
    [game addBtn:_btn3];
    [game addBtn:_btn4];
    
    [game addMenu:menu];
    [game setState:state];
}

- (void)prepState {
    state = [[StateButton alloc] init];
    [state construct:_center];
}

- (void)prepMenu {
    menu = [[Menu alloc] init];
    [menu construct:_menuview];
    [menu setLabel:_label];
}

- (IBAction)tap:(id)sender {
    [game analyseTap: (int)[sender tag]];
}

- (IBAction)retry:(id)sender {
    [game retryLevel];
}

- (IBAction)restart:(id)sender {
    [game start];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
