//
//  Game.m
//  taptap
//
//  Created by Alain Pitre on 2016-01-17.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"
#import "TapButton.h"

@implementation Game {
    int nbButton;
    Menu* menu;
    StateButton* state;
    TapButton* active;
    TapButton* listBtn[4];
    int tap[2000];
    int level;
    int current;
}

- (void) addMenu:(Menu*) action {
    menu = action;
}

- (void) setState: (StateButton*) uibutton {
    state = uibutton;
}

-(void) start {
    [menu hide];
    level = 1;
    current = 1;
    [state setLevel:level];
    tap[level] = [self getRandom];
    [self stepTo];
}

- (void) retryLevel {
    [state setLevel:level];
    [menu hide];
    current = 1;
    [self stepTo];
}

- (void) nextLevel {
    level += 1;
    current = 1;
    tap[level] = [self getRandom];
    [self stepTo];
}

- (int) getRandom {
    return arc4random() % 4;
}

- (void) addBtn: (UIButton*) btn {
    if (nbButton < 4) {
        TapButton* tapButton = [[TapButton alloc] init];
        [btn setTag:nbButton];
        [tapButton setButton:btn];
        listBtn[nbButton] = tapButton;
        nbButton++;
    }
}

- (void) analyseTap: (int) tag {
    if ([state isLock]) {
        NSLog(@"LEVEL LOCK \n");
    } else if (tap[current] != tag) {
        [menu showError];
    } else {
        [self tap:tag];
        if (current > level) {
            [state setLevel:current];
            [self performSelector:@selector(nextLevel) withObject:nil afterDelay:1];
        }
    }
}

- (void) tapLevel {
    [self tap: tap[current]];
    [self performSelector:@selector(stepTo) withObject:nil afterDelay:0.20];
}

- (void) stepTo {
    [active desactivate];
    if (current <= level) {
        [self performSelector:@selector(tapLevel) withObject:nil afterDelay:0.50];
    } else {
        current = 1;
        [state setPlay];
    }
}

-(void) tap: (int) tag {
    [active desactivate];
    active = listBtn[tag];
    [active activate];
    current++;
}

@end