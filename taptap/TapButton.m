//
//  TapButton.m
//  taptap
//
//  Created by Alain Pitre on 2016-01-17.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "TapButton.h"

@implementation TapButton {
    UIButton* uiButton;
    AVAudioPlayer *_audioPlayer;
}

-(void) setSound {
    int tag = (int) [uiButton tag];
    NSString *path = [NSString stringWithFormat:@"%@/sounds/sound_%d.wav", [[NSBundle mainBundle] resourcePath], tag];
    
    NSLog(@"%@\n", path);
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
}

-(void) setButton: (UIButton*) btn {
    uiButton = btn;
    [uiButton addTarget:self action:@selector(activate) forControlEvents:UIControlEventTouchDown];
    [uiButton addTarget:self action:@selector(desactivate) forControlEvents:UIControlEventTouchUpInside];
    [self desactivate];
    [self setSound];
}

- (void) activate {
    [uiButton setAlpha:1];
    
    if ([_audioPlayer isPlaying]) {
        [_audioPlayer pause];
        [_audioPlayer setCurrentTime:0.0];
    }
    
    [_audioPlayer play];
    
}

- (void) desactivate {
    [uiButton setAlpha:0.5];
}

@end