//
//  TapButton.h
//  taptap
//
//  Created by Alain Pitre on 2016-01-17.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef TapButton_h
#define TapButton_h

#import <UIKit/UIKit.h>

@interface TapButton : NSObject

- (void) setButton: (UIButton*) btn;

- (void) activate;

- (void) desactivate;

@end


#endif /* TapButton_h */
