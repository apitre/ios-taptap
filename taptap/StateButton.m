//
//  StateButton.m
//  taptap
//
//  Created by Alain Pitre on 2016-01-18.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StateButton.h"

@implementation StateButton {
    UIButton* state;
    int size;
    BOOL lock;
}

- (void) construct: (UIButton*) btn {
    state = btn;
    size = btn.frame.size.height;
    state.layer.cornerRadius = (size / 2);
}

- (void) lock {
    lock = true;
}

- (void) unlock {
    lock = false;
}

- (BOOL) isLock {
    return lock;
}

- (void) setLevel: (int) level {
    [state setTitle:[NSString stringWithFormat:@"%d", level] forState:UIControlStateNormal];
}

- (void) setPlay {
    [state setTitle:@"GO" forState:UIControlStateNormal];
    lock = false;
}

- (void) setRun {
    [state setTitle:@"" forState:UIControlStateNormal];
    [self lock];
}

- (void) setGood {
    [state setTitle:@"YÉ" forState:UIControlStateNormal];
    [self lock];
}

- (void) setError {
    [state setTitle:@"ARK" forState:UIControlStateNormal];
    [self lock];
}

@end
