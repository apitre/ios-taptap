//
//  Game.h
//  taptap
//
//  Created by Alain Pitre on 2016-01-17.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef Game_h
#define Game_h

#import <UIKit/UIKit.h>
#import "Menu.h"
#import "StateButton.h"

@interface Game : NSObject

- (void) setState: (StateButton*) uibutton;

- (void) addBtn: (UIButton*) btn;

- (void) addMenu:(Menu*) action;

- (void) analyseTap: (int) tag;

- (void) start;

- (void) nextLevel;

- (void) retryLevel;

@end


#endif /* Game_h */
