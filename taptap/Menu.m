//
//  Menu.m
//  taptap
//
//  Created by Alain Pitre on 2016-01-19.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Menu.h"

@implementation Menu {
    UIView* menu;
    UILabel* label;
}

- (void) construct: (UIView*) view {
    menu = view;
}

- (void) setLabel: (UILabel*) text {
    label = text;
}

- (void) hide {
    [menu setHidden:true];
}

- (void) show {
    [menu setHidden:false];
}

- (void) showError {
    label.text = @"LEVEL FAILED!";
    [self show];
}

@end
