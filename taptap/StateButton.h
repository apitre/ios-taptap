//
//  StateButton.h
//  taptap
//
//  Created by Alain Pitre on 2016-01-18.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef StateButton_h
#define StateButton_h

#import <UIKit/UIKit.h>

@interface StateButton : NSObject

- (void) construct: (UIButton*) btn;

- (void) setError;

- (void) setGood;

- (void) lock;

- (void) setLevel: (int) level;

- (void) setRun;

- (void) setPlay;

- (void) unlock;

- (BOOL) isLock;

@end


#endif /* StateButton_h */
