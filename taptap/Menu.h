//
//  Menu.h
//  taptap
//
//  Created by Alain Pitre on 2016-01-19.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef Menu_h
#define Menu_h

#import <UIKit/UIKit.h>

@interface Menu : NSObject

- (void) construct: (UIView*) view;

- (void) hide;

- (void) setLabel: (UILabel*) text;

- (void) show;

- (void) showError;

@end

#endif /* Menu_h */
